public class UpdateActive_InactiveCreditCardsClass {
    
    public void updateAfterInsert(String c_Id,String status) {
        Customer__c customer = [SELECT Total_Number_of_Active_credit_card__c,
                            Total_Number_of_InActive_Credit_card__c FROM Customer__c 
                                    WHERE Id=:c_Id];
                                    
        try{
            if(customer != NULL) {
                if(status=='YES'){
                    customer.Total_Number_of_Active_credit_card__c +=1;
                }
                if(status== 'NO'){
                    customer.Total_Number_of_InActive_Credit_card__c+=1;
                }
            }
            update customer;
        }
        catch(Exception e) {
           throw e;
        }
        
}

    public void updateAfterEditing(String c_Id,String c_old_Id,
                                                    String new_Status,String old_Status) {
        Customer__c newCustomer = [SELECT Id,Total_Number_of_Active_credit_card__c,
            Total_Number_of_InActive_Credit_card__c FROM Customer__c 
                                    WHERE Id=:c_Id];
                                    
        Customer__c oldCustomer = [SELECT Id,Total_Number_of_Active_credit_card__c,
            Total_Number_of_InActive_Credit_card__c FROM Customer__c 
                                    WHERE Id=:c_old_Id];
        try{
            if((newCustomer != NULL) && (oldCustomer != NULL)) {
                
                if(newCustomer.Id != oldCustomer.Id) {
                    if(new_Status==old_Status && new_status=='NO'){
                         oldCustomer.Total_Number_of_InActive_Credit_card__c -=1;
                         newCustomer.Total_Number_of_InActive_Credit_card__c +=1;          
                    }
                    if(new_Status==old_Status && new_status=='YES'){
                       oldCustomer.Total_Number_of_Active_credit_card__c -=1;
                       newCustomer.Total_Number_of_Active_credit_card__c +=1;                 
                    }
                    if(old_Status=='YES' && new_Status=='NO'){
                        oldCustomer.Total_Number_of_Active_credit_card__c -=1;
                        newCustomer.Total_Number_of_InActive_Credit_card__c +=1;
                    }
                    if(old_Status=='NO' && new_Status=='YES'){
                        newCustomer.Total_Number_of_Active_credit_card__c +=1;
                        oldCustomer.Total_Number_of_InActive_Credit_card__c -=1;
                    }
                }
                else {
                    if(old_Status=='NO' && new_Status=='YES') {
                        newCustomer.Total_Number_of_InActive_Credit_card__c -=1;
                        newCustomer.Total_Number_of_Active_credit_card__c +=1;
                    }
                    if(old_Status=='YES' && new_Status=='NO') {
                        newCustomer.Total_Number_of_InActive_Credit_card__c +=1;
                        newCustomer.Total_Number_of_Active_credit_card__c -=1;
                    }
                }
            }
            
            update oldCustomer;
            update newCustomer;
        }
        catch(Exception e) {
           throw e;
        }
    }
    public void updateAfterDelete(String c_Id,String status) {
        Customer__c customer = [SELECT Total_Number_of_Active_credit_card__c,
        Total_Number_of_InActive_Credit_card__c FROM Customer__c WHERE Id=:c_Id];
        try{
            if(customer != NULL) {
                if(status=='YES'){
                    customer.Total_Number_of_Active_credit_card__c -=1;
                  }
                 if(status=='NO'){
                     customer.Total_Number_of_InActive_Credit_card__c-=1;
                 }
            }
            update customer;
        }
        catch(Exception e) {
           throw e;
        }
    }
}
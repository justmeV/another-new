//Update class from Thejas
public class UpdateNoOfCreditCards {
    public void updateAfterInsert(String c_Id) {
        Customer__c customer = [SELECT Total_number_of_credit_card__c FROM Customer__c 
                                    WHERE Id=:c_Id];
        try{
            if(customer != NULL) {
                
                customer.Total_number_of_credit_card__c +=1;
            }
            update customer;
        }
        catch(Exception e) {
           
        }
    }
    
    public void updateAfterEditing(String c_Id,String c_old_Id) {
        Customer__c newCustomer = [SELECT Total_number_of_credit_card__c FROM Customer__c 
                                    WHERE Id=:c_Id];
                                    
        Customer__c oldCustomer = [SELECT Total_number_of_credit_card__c FROM Customer__c 
                                    WHERE Id=:c_old_Id];
        try{
            if((newCustomer != NULL) && (oldCustomer != NULL) && newCustomer != oldCustomer) {
                newCustomer.Total_number_of_credit_card__c +=1;
                oldCustomer.Total_number_of_credit_card__c -=1;
            }
            update oldCustomer;
            update newCustomer;
        }
        catch(Exception e) {
           
        }
    }
    
    public void updateAfterDelete(String c_Id) {
        Customer__c customer = [SELECT Total_number_of_credit_card__c FROM Customer__c 
                                    WHERE Id=:c_Id];
        try{
            if(customer != NULL) {
                
                customer.Total_number_of_credit_card__c -=1;
            }
            update customer;
        }
        catch(Exception e) {
           
        }
    }
}
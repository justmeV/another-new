public with sharing Class StatusBarComponentCtrl
{
    public string objectTypeName{get;set;}
    public RecordType oRecordType{get;set;}
    public string record{get;set;}
    public string fieldName{get;set;}
    public string fieldValue{get;set;}
    public list<list<StatusWrapper>> listOfListStatusWrapper{get;set;}
    public boolean isConfigAvailable{get;set;}

    public StatusBarComponentCtrl() 
    {
        listOfListStatusWrapper = new list<list<StatusWrapper>>();
        isConfigAvailable = false;
    }

    public void componentAction()
    {
        String sRecordType = 'All';
        Recordtype RecordTypeObj;
        
        if(oRecordType != NULL)
        {
            RecordtypeObj = [SELECT id,name from recordType WHERE id=:oRecordType.id];
            sRecordType  = RecordtypeObj.name;
        }
        
        String sQuery = 'SELECT Color_Completed_Stage__c,Color_Current_Stage__c,Color_Pending_Stage__c,Target_Field_API_Name__c,Target_Field_Name__c,';
        sQuery =sQuery+'Target_Field_Values__c,Target_Object_API_Name__c,Target_Object_Name__c,Target_Object_Record_Type__c FROM Flow_Configuration__c ';
        sQuery =sQuery+'WHERE Target_Object_API_Name__c =:objectTypeName AND Is_Active__c=true AND Target_Field_API_Name__c =:fieldName AND Target_Object_Record_Type__c =:sRecordtype';
        
        System.debug('****'+sQuery);
        
        List<Flow_Configuration__c> listConfiguration = Database.query(sQuery);
        
        if(!listConfiguration.isEmpty())
        {
            isConfigAvailable= true;
            integer FieldCount = 0;
            list<StatusWrapper> listStatusWrapper = new list<StatusWrapper>();
            Boolean completedStageBoolean = true;
            Integer statusRank = 1;   
    
            for(String sPicklistValue : listConfiguration[0].Target_Field_Values__c.split(';'))
            {
                boolean startStatus = false;
                boolean endStatus = false;
                if(statusRank == 1)
                    startStatus = true;
                if(statusRank == listConfiguration[0].Target_Field_Values__c.split(';').size()) 
                    endStatus = true;
              
                if(!sPicklistValue.trim().equalsIgnoreCase(FieldValue))
                {
                    if(completedStageBoolean)
                        listStatusWrapper.add(new StatusWrapper(sPicklistValue,listConfiguration[0].Color_Completed_Stage__c,startStatus,endStatus));
                    else
                        listStatusWrapper.add(new StatusWrapper(sPicklistValue,listConfiguration[0].Color_Pending_Stage__c,startStatus,endStatus));
                }
                else
                {
                    String currentColor = listConfiguration[0].Color_Current_Stage__c;
                    
                    if(statusRank == listConfiguration[0].Target_Field_Values__c.split(';').size())
                    currentColor = listConfiguration[0].Color_Completed_Stage__c;
                    
                    listStatusWrapper.add(new StatusWrapper(sPicklistValue,currentColor,startStatus,endStatus));
                    completedStageBoolean = false;
                }
                FieldCount++;
                if(FieldCount==100)
                {
                    listOfListStatusWrapper.add(listStatusWrapper);
                    listStatusWrapper = new list<StatusWrapper>();
                    FieldCount = 0;
                }
                statusRank++;
            }
            if(FieldCount > 0)
                listOfListStatusWrapper.add(listStatusWrapper);
        }
        else
        {
            isConfigAvailable = false;
        }
    }
    
    public class StatusWrapper
    {
        public String statusText{get;set;}
        public string statusColor{get;set;}
        public String type{get;set;}
        
        public StatusWrapper(String sText, String sColor,boolean startStatus, boolean endStatus)
        {
            statusText = sText;
            statusColor = sColor;
            type = 'midStatus';
            
            if(startStatus && endStatus)
                type = 'onlyStatus';
            else if(startStatus)
                type = 'firstStatus';
            else if(endStatus)
                type = 'lastStatus';
        }
    }
}
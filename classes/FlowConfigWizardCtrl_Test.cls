@isTest
private class FlowConfigWizardCtrl_Test
{
    @testSetup
    static void dataSetup() 
    {
        list<flow_configuration__c> listFlowConfigs = new list<flow_configuration__c>();
        
        flow_configuration__c oFlow1 = new flow_configuration__c();
        oFlow1.Is_Active__c = true;
        oFlow1.Color_Completed_Stage__c = 'Green';
        oFlow1.Color_Current_Stage__c = 'Blue';
        oFlow1.Color_Pending_Stage__c = 'Red';
        oFlow1.Target_Field_API_Name__c = 'AccountSource';
        oFlow1.Target_Field_Name__c = 'Account Source';
        oFlow1.Target_Field_Values__c = 'Web;other;phone enquiry';
        oFlow1.Target_Object_API_Name__c = 'Account';
        oFlow1.Target_Object_Name__c= 'Account';
        oFlow1.Target_Object_Record_Type__c = 'All';
        listFlowConfigs.add(oFlow1);
        
        flow_configuration__c oFlow2 = new flow_configuration__c();
        oFlow2.Is_Active__c = true;
        oFlow2.Color_Completed_Stage__c = 'Green';
        oFlow2.Color_Current_Stage__c = 'Blue';
        oFlow2.Color_Pending_Stage__c = 'Red';
        oFlow2.Target_Field_API_Name__c = 'CleanStatus';
        oFlow2.Target_Field_Name__c = 'Clean Status';
        oFlow2.Target_Field_Values__c = 'Web;other;phone enquiry';
        oFlow2.Target_Object_API_Name__c = 'Account';
        oFlow2.Target_Object_Name__c= 'Account';
        oFlow2.Target_Object_Record_Type__c = 'All';
        listFlowConfigs.add(oFlow2);
        
        flow_configuration__c oFlow3 = new flow_configuration__c();
        oFlow3.Is_Active__c = true;
        oFlow3.Color_Completed_Stage__c = 'Green';
        oFlow3.Color_Current_Stage__c = 'Blue';
        oFlow3.Color_Pending_Stage__c = 'Red';
        oFlow3.Target_Field_API_Name__c = 'Industry';
        oFlow3.Target_Field_Name__c = 'Industry';
        oFlow3.Target_Field_Values__c = 'Web;other;phone enquiry';
        oFlow3.Target_Object_API_Name__c = 'Account';
        oFlow3.Target_Object_Name__c= 'Account';
        oFlow3.Target_Object_Record_Type__c = 'All';
        listFlowConfigs.add(oFlow3);
        
        flow_configuration__c oFlow4 = new flow_configuration__c();
        oFlow4.Is_Active__c = true;
        oFlow4.Color_Completed_Stage__c = 'Green';
        oFlow4.Color_Current_Stage__c = 'Blue';
        oFlow4.Color_Pending_Stage__c = 'Red';
        oFlow4.Target_Field_API_Name__c = 'Ownership';
        oFlow4.Target_Field_Name__c = 'Ownership';
        oFlow4.Target_Field_Values__c = 'Web;other;phone enquiry';
        oFlow4.Target_Object_API_Name__c = 'Account';
        oFlow4.Target_Object_Name__c= 'Account';
        oFlow4.Target_Object_Record_Type__c = 'All';
        listFlowConfigs.add(oFlow4);
        
        flow_configuration__c oFlow5 = new flow_configuration__c();
        oFlow5.Is_Active__c = true;
        oFlow5.Color_Completed_Stage__c = 'Green';
        oFlow5.Color_Current_Stage__c = 'Blue';
        oFlow5.Color_Pending_Stage__c = 'Red';
        oFlow5.Target_Field_API_Name__c = 'Rating';
        oFlow5.Target_Field_Name__c = 'Rating';
        oFlow5.Target_Field_Values__c = 'Web;other;phone enquiry';
        oFlow5.Target_Object_API_Name__c = 'Account';
        oFlow5.Target_Object_Name__c= 'Account';
        oFlow5.Target_Object_Record_Type__c = 'All';
        listFlowConfigs.add(oFlow5);
        
        flow_configuration__c oFlow6 = new flow_configuration__c();
        oFlow6.Is_Active__c = true;
        oFlow6.Color_Completed_Stage__c = 'Green';
        oFlow6.Color_Current_Stage__c = 'Blue';
        oFlow6.Color_Pending_Stage__c = 'Red';
        oFlow6.Target_Field_API_Name__c = 'Type';
        oFlow6.Target_Field_Name__c = 'Type';
        oFlow6.Target_Field_Values__c = 'Web;other;phone enquiry';
        oFlow6.Target_Object_API_Name__c = 'Account';
        oFlow6.Target_Object_Name__c= 'Account';
        oFlow6.Target_Object_Record_Type__c = 'All';
        listFlowConfigs.add(oFlow6);
        
        insert listFlowConfigs;
        
    }
    static testMethod void newFlowConfigFunctionalityTest()
    {
        
            FlowConfigWizardCtrl oFConfigControl = new FlowConfigWizardCtrl ();
            oFConfigControl.getAllObjectList();
            oFConfigControl.selectedTargetObject = 'Account';
            oFConfigControl.selectedTargetObjectApi = 'Account';
            oFConfigControl.selectObjectStage();
            oFConfigControl.selectedTargetObjectApi = 'Account';
            oFConfigControl.getObjectRecordTypeList();
            oFConfigControl.selectedRecordType = 'All';
            oFConfigControl.selectRecordTypeStage();
            oFConfigControl.getAllPicklistsList();
            oFConfigControl.selectedPicklistField = 'Type';
            oFConfigControl.selectFieldStage();
            oFConfigControl.selectedPicklistField = 'SLA';
            oFConfigControl.selectFieldStage();
        Test.startTest();
            oFConfigControl.picklistRightValues = 'Gold;Silver';   
            oFConfigControl.generatePage();
            oFConfigControl.PageName = '';
            oFConfigControl.saveConfiguration();
            oFConfigControl.PageName = 'testPage$';
            oFConfigControl.nextPage();
            oFConfigControl.previousPage();
            oFConfigControl.lastPage();
            oFConfigControl.firstPage();
            oFConfigControl.saveConfiguration();
            oFConfigControl.PageName = 'testPage';
            oFConfigControl.saveConfiguration();
            list<flow_configuration__c> lstNewConfig = [select id from flow_configuration__c];
            system.assertEquals(7,lstNewConfig.size());
        Test.stopTest();
    }
    
    static testMethod void editFlowConfigFunctionalityTest()
    {
        PageReference pageRef = Page.FlowConfigWizard;
        Test.setCurrentPage(pageRef);
        
        
        FlowConfigWizardCtrl oFConfigControl = new FlowConfigWizardCtrl ();
        oFConfigControl.getAllObjectList();
        oFConfigControl.selectedTargetObject = 'Account';
        oFConfigControl.selectedTargetObjectApi = 'Account';
        oFConfigControl.selectObjectStage();
        pageRef.getParameters().put('configId', oFConfigControl.flowConfigList[0].id);
        String sId = oFConfigControl.flowConfigList[0].id;
        //oFConfigControl.configId = oFConfigControl.flowConfigList[0].id;
        oFConfigControl.editExistingConfiguration();
        oFConfigControl.picklistRightValues  = 'Web;Other';
        Test.startTest();
        oFConfigControl.updateConfig();
        list<flow_configuration__c> lstNewConfig = [SELECT id,Target_Field_Values__c FROM flow_configuration__c WHERE id=:sId];
        system.assertEquals('Web;Other',lstNewConfig[0].Target_Field_Values__c);
        //oFConfigControl.finishPageConfiguration();
        Test.stopTest();
    }
}
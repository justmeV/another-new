global class PolicyWebService {
    webService static Policy__c[]  getpolicydetails(String Policynum) {

        
       Policy__c[] p = [SELECT Id,Policy_Expiration_Date__c,Policy_Holder_Name__c,Policy_Name__c FROM Policy__c WHERE Name= :Policynum];

       
       return p;    
    }

}
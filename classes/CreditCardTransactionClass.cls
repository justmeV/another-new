public class CreditCardTransactionClass 
{
    public List<Product2> UserTemp = new List<Product2>();
    public CreditCardTransactionClass ()
    {
        
    }
    private final ApexPages.StandardController controller;
    public CreditCardTransactionClass (ApexPages.StandardController controller)
    {
        this.controller = controller;
    }
    public List<SelectOption> ProductList
    {
        get
        {
            UserTemp = [Select Name From Product2];
            
            ProductList = new List<SelectOption>();
            
            for(Product2 temp : UserTemp)
            {
                ProductList.add(new SelectOption(temp.Name, temp.Name));
            }
            return ProductList;
        }
        set;
    }
}
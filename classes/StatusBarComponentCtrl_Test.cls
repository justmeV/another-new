@isTest
private class StatusBarComponentCtrl_Test
{
    static testMethod void generateIndicatorTest()
    {
        Account oAccount = new Account();
        oAccount.name = 'TestAccount';
        oAccount.Type = 'Customer - Channel';
        insert oAccount;
        
        Flow_Configuration__c oFlow = new Flow_Configuration__c();
        oFlow.Is_Active__c = true;
        oFlow.Color_Completed_Stage__c = 'Green';
        oFlow.Color_Current_Stage__c= 'Blue';
        oFlow.Color_Pending_Stage__c= 'Red';
        oFlow.Target_Field_API_Name__c = 'type';
        oFlow.Target_Field_Name__c = 'type';
        oFlow.Target_Object_API_Name__c = 'Account';
        oFlow.Target_Object_Name__c = 'Account';
        oFlow.Target_Object_Record_Type__c = 'All';
        oFlow.Target_Field_Values__c = 'Prospect;Customer - Channel;Installation Partner;Other';
        insert oFlow;
        
        Test.startTest();
            StatusBarComponentCtrl oSBC = new StatusBarComponentCtrl();
            oSBC.objectTypeName = 'Account';
            oSBC.fieldName = 'Type';
            oSBC.fieldValue = 'Customer - Channel';
            oSBC.record = oAccount.id;
            oSBC.componentAction();
            System.assertEquals(true,oSBC.isConfigAvailable);
        Test.stopTest();
        
    }
    
    static testMethod void flowNotAvailable()
    {
        Account oAccount = new Account();
        oAccount.name = 'TestAccount';
        oAccount.Type = 'Customer - Channel';
        insert oAccount;
        
        
        Test.startTest();
            StatusBarComponentCtrl oSBC = new StatusBarComponentCtrl();
            oSBC.objectTypeName = 'Account';
            oSBC.fieldName = 'Type';
            oSBC.fieldValue = 'Customer - Channel';
            oSBC.record = oAccount.id;
            oSBC.componentAction();
            System.assertEquals(false,oSBC.isConfigAvailable);
        Test.stopTest();
        
    }
}
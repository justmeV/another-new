public class UpdateActiveCreditCardLimit{
   public void updateAfterDeleteCard(String c_Id,String cc_Id){
     Customer__c customer = [SELECT Sum_of_Active_credit_card_limit__c FROM Customer__c 
                                    WHERE Id=:c_Id];
                                    
     Credit_Card__c creditCard=[SELECT  Card_Limit__c,IsActive__c FROM Credit_Card__c WHERE Id=:cc_Id];                                
       try{
            if(customer != NULL) {
              if(creditCard.IsActive__c=='Yes'){
                
                customer.Sum_of_Active_credit_card_limit__c =0;
            }
            }
            update customer;
        }
        catch(Exception e) {
           
        }
   
   }
   
   public void updateAfterInsertCard(String c_Id,String cc_Id){
     Customer__c customer = [SELECT Sum_of_Active_credit_card_limit__c FROM Customer__c 
                                    WHERE Id=:c_Id];
     Credit_Card__c creditCard=[SELECT  Card_Limit__c,IsActive__c FROM Credit_Card__c WHERE Id=:cc_Id];                               
     try{
            if(customer != NULL) {
               if(creditCard.IsActive__c=='Yes'){
                
                customer.Sum_of_Active_credit_card_limit__c =creditCard.Card_Limit__c ;
                  }
            }
            update customer;
        }
        catch(Exception e) {
           
        }
   
   }
   
   public void updateAfterEditCard(String c_Id,String c_old_Id,String cc_Id,String cc_Old_Id){
     Customer__c customer = [SELECT Id,Sum_of_Active_credit_card_limit__c FROM Customer__c 
                                    WHERE Id=:c_Id];
                                    
     Customer__c old_customer = [SELECT Id,Sum_of_Active_credit_card_limit__c FROM Customer__c 
                                    WHERE Id=:c_old_Id]; 
                                                                  
     Credit_Card__c creditCard=[SELECT  Card_Limit__c,IsActive__c FROM Credit_Card__c WHERE Id=:cc_Id];
     Credit_Card__c old_CreditCard=[SELECT  Card_Limit__c,IsActive__c FROM Credit_Card__c WHERE Id=:cc_Old_Id];
     
      try{
            if(customer != NULL && old_customer != NULL) {
                old_customer.Sum_of_Active_credit_card_limit__c =0 ;
                update old_customer;
                if(creditCard.IsActive__c=='Yes'){
                    
                    customer.Sum_of_Active_credit_card_limit__c=creditCard.Card_Limit__c;
                    update customer;
               }
            }
            
            
        }
        catch(Exception e) {
           
        }
   
   }



}
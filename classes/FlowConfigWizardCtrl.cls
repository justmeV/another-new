public with sharing class FlowConfigWizardCtrl 
{
    public boolean isRecordtypeAvailable{get;set;}
    public string selectedTargetObject{get;set;}     
    public string selectedTargetObjectApi{get;set;}
    public string selectedRecordType{get;set;}
    public string selectedPicklistField{get;set;}
    public string recordtypeLabel{get;set;}
    public string picklistLeftValues{get;set;}
    public string picklistAllRightValues{get;set;}
    public string picklistRightValues{get;set;}
    public Flow_Configuration__c objFlowConfig{get;set;}     
    public list <Flow_Configuration__c> flowConfigList{get;set;}
    public boolean showPageCode{get;set;}
    public string pageCode{get;set;}
    public string pageName{get;set;}
    public list<ApexPage> lstExistingApexPage{get;set;}
    public boolean isEdit{get;set;}
    
    public list <selectOption> allObjectList;
    public list <selectOption> objectRecordTypeList;
    public list <selectOption> allPicklistsList;
    public list <selectOption> picklistValuesList;

    /*** for pagination ***/
    public Boolean disableNext{get;set;}
    public Boolean disablePrevious{get;set;}
    public Integer currentPage{get;set;}
    public Integer TotalRecords{get;set;}
    public Integer TotalPages{get;set;}
    
    private list <Flow_Configuration__c> listTotalConfigurations;
    private map <string, string> objectLabelObjectNameMap;
    private map <string, string> fieldApiFieldNameMap;
    private map <string, string> recordTypeIdNameMap;
    private map <string, Schema.SObjectType> objectNameObjectTypeMap;
    private map<Id,Flow_Configuration__c> mapIdExistingFlowConfig;    

    public FlowConfigWizardCtrl()
    {
        try
        {
            isEdit = false;
            listTotalConfigurations = new list < Flow_Configuration__c > ();
            lstExistingApexPage = new list < ApexPage > ();
            objFlowConfig = new Flow_Configuration__c();
            objectNameObjectTypeMap = Schema.getGlobalDescribe();
            objectLabelObjectNameMap = new map < string, string > ();
            fieldApiFieldNameMap = new map < string, string > ();
            recordTypeIdNameMap = new map < string, string > ();
            flowConfigList = new list < Flow_Configuration__c > ();
            objFlowConfig = new Flow_Configuration__c();
            isRecordtypeAvailable = false;
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void FetchConfigs()
    {
        try
        {
            listTotalConfigurations = [SELECT Id, Target_Object_API_Name__c, Target_Field_API_Name__c, Is_Active__c, 
                                        Color_Completed_Stage__c, Color_Current_Stage__c, Color_Pending_Stage__c, Target_Field_Values__c, 
                                        Target_Field_Name__c, Target_Object_Name__c, Target_Object_Record_Type__c, Linked_Page__c
                                        FROM Flow_Configuration__c
                                        WHERE Target_Object_API_Name__c =: selectedTargetObjectApi];
                
            if (!listTotalConfigurations.isEmpty())
            {
                mapIdExistingFlowConfig = new map < Id, Flow_Configuration__c > ();
                for (Flow_Configuration__c oFlow: listTotalConfigurations)
                {
                    mapIdExistingFlowConfig.put(oFlow.id, oFlow);
                }
            }
            bindDataToTable(1);
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    private void bindDataToTable(Integer newPageIndex)
    {
        try
        {
            integer pageSize = 5;
            flowConfigList = new list < Flow_Configuration__c > ();
            integer startParentNum = ((newPageIndex - 1) * pageSize) + 1;
            integer endParentNum = (newPageIndex * pageSize) - 1;
            if (listTotalConfigurations.size() < (newPageIndex * pageSize))
            {
                endParentNum = listTotalConfigurations.size() - 1;
            }
            for (integer i = (startParentNum - 1); i <= endParentNum; i++)
            {
                flowConfigList.add(listTotalConfigurations[i]);
            }
            currentPage = newPageIndex;
            Integer mod = 0;
            TotalRecords = listTotalConfigurations.size();
            TotalPages = integer.valueof(listTotalConfigurations.size() / pageSize);
            mod = listTotalConfigurations.size() - (TotalPages * pageSize);
            if (mod > 0) TotalPages++;
            disableNext = ((currentPage * pageSize) >= listTotalConfigurations.size());
            disablePrevious = !(currentPage > 1);
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void previousPage()
    {
        try
        {
            bindDataToTable(currentPage - 1);
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void nextPage()
    {
        try
        {
            bindDataToTable(currentPage + 1);
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void lastPage()
    {
        try
        {
            bindDataToTable(TotalPages);
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void firstPage()
    {
        try
        {
            bindDataToTable(1);
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public list < selectOption > getAllObjectList()
    {
        allObjectList = new list < selectOption > ();
        allObjectList.add(new SelectOption('', '--None--'));
        for (Schema.SObjectType objTyp: Schema.getGlobalDescribe().Values())
        {
            if (objTyp.getDescribe().isCreateable() && objTyp.getDescribe().isDeletable() && objTyp.getDescribe().isUndeletable() && !objTyp.getDescribe().isCustomSetting())
            {
                allObjectList.add(new SelectOption(objTyp.getDescribe().getName(), objTyp.getDescribe().getLabel()));
                objectLabelObjectNameMap.put(objTyp.getDescribe().getName(), objTyp.getDescribe().getName());
            }
        }
        allObjectList.sort();
        RETURN allObjectList;
    }
    
    public list < selectOption > getObjectRecordTypeList()
    {
        objectRecordTypeList = new list < selectOption > ();
        objectRecordTypeList.add(new SelectOption('All', '--All--'));
        if (selectedTargetObjectApi != NULL && selectedTargetObjectApi != '')
        {
            Schema.SObjectType thisSObject = objectNameObjectTypeMap.get(selectedTargetObjectApi);
            if (thisSObject != NULL)
            {
                List < Schema.RecordTypeInfo > recordTypeInfoList = thisSObject.getDescribe().getRecordTypeInfos();
                for (Schema.RecordTypeInfo objRecordTypeInfo: recordTypeInfoList)
                {
                    if(objRecordTypeInfo.getName()!='Master')
                    {
                        objectRecordTypeList.add(new SelectOption(objRecordTypeInfo.getRecordTypeId(), objRecordTypeInfo.getName()));
                        recordTypeIdNameMap.put(objRecordTypeInfo.getRecordTypeId(), objRecordTypeInfo.getName());
                    }
                }
            }
        }
        RETURN objectRecordTypeList;
    }
    
    public list < selectOption > getAllPicklistsList()
    {
        allPicklistsList = new list < selectOption > ();
        allPicklistsList.add(new SelectOption('', '--None--'));
        if (selectedTargetObjectApi != NULL && selectedTargetObjectApi != '')
        {
            Schema.SObjectType thisSObject = objectNameObjectTypeMap.get(selectedTargetObjectApi);
            if (thisSObject != NULL)
            {
                Map < String, Schema.SObjectField > fieldMap = thisSObject.getDescribe().fields.getMap();
                for (Schema.SObjectField objField: fieldMap.values())
                {
                    if (string.valueOf(objField.getDescribe().getType()) == 'Picklist')
                    {
                        allPicklistsList.add(new SelectOption(objField.getDescribe().getName(), objField.getDescribe().getLabel()));
                        fieldApiFieldNameMap.put(objField.getDescribe().getName(), objField.getDescribe().getLabel());
                    }
                }
            }
            allPicklistsList.sort();
        }
        RETURN allPicklistsList;
    }
    
    public void SelectObjectStage()
    {
        try
        {
            if (selectedTargetObject != NULL && selectedTargetObject != '')
            {
                selectedTargetObjectApi = objectLabelObjectNameMap.get(selectedTargetObject);
                fetchConfigs();
            }
            else selectedTargetObjectApi = NULL;
            selectedRecordType = NULL;
            selectedPicklistField = NULL;
            generatePageName();
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void SelectRecordTypeStage()
    {
        try
        {
            selectedPicklistField = NULL;
            fetchConfigs();
            generatePageName();
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void SelectFieldStage()
    {
        try
        {
            picklistLeftValues = '';
            picklistRightValues = '';
            picklistAllRightValues = '';
            Schema.SObjectType thisSObject = objectNameObjectTypeMap.get(selectedTargetObjectApi);
            if (thisSObject != NULL && selectedPicklistField != NULL && selectedPicklistField != '')
            {
                Schema.SObjectField selectedField = thisSObject.getDescribe().fields.getMap().get(selectedPicklistField);
                for (Schema.PicklistEntry objPicklistItem: selectedField.getDescribe().getPicklistValues())
                {
                    picklistLeftValues += objPicklistItem.getLabel() + '|' + objPicklistItem.getValue() + ';';
                }
            }
            fetchConfigs();
            generatePageName();
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public pagereference SaveConfiguration()
    {
        try
        {
            boolean errorConfigurationExists = false;
            objFlowConfig.Target_Object_Name__c = selectedTargetObject;
            objFlowConfig.Target_Object_API_Name__c = selectedTargetObjectApi;
            if (selectedRecordType == 'All') objFlowConfig.Target_Object_Record_Type__c = 'All';
            else objFlowConfig.Target_Object_Record_Type__c = recordTypeIdNameMap.get(selectedRecordType);
            objFlowConfig.Target_Field_Name__c = fieldApiFieldNameMap.get(selectedPicklistField);
            objFlowConfig.Target_Field_API_Name__c = selectedPicklistField;
            objFlowConfig.Target_Field_Values__c = picklistRightValues;
            if (!listTotalConfigurations.isEmpty())
            {
                for (Flow_Configuration__c oFlow: listTotalConfigurations)
                {
                    if (objFlowConfig.Target_Object_Record_Type__c == oFlow.Target_Object_Record_Type__c && objFlowConfig.Target_Object_API_Name__c == oFlow.Target_Object_API_Name__c && objFlowConfig.Target_Field_API_Name__c == oFlow.Target_Field_API_Name__c)
                    {
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Similar configuration already exists.'));
                        RETURN NULL;
                    }
                }
            }
            if (pageName != NULL && pageName.trim().length() < 1)
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, LABEL.PAGE_NAME_REQUIRED_MESSAGE);
                ApexPages.addMessage(myMsg);
                RETURN NULL;
            }
            else if (!Pattern.matches('^[a-zA-Z0-9_]+$', pageName))
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, LABEL.PAGE_NAME_SPECIAL_CHARACTER_VALIDATION);
                ApexPages.addMessage(myMsg);
                RETURN NULL;
            }
            
            lstExistingApexPage = [SELECT Id, Name FROM ApexPage WHERE Name =: pageName LIMIT 1];
            
            if (!lstExistingApexPage.isEmpty())
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, LABEL.PAGE_ALREADY_EXISTS);
                ApexPages.addMessage(myMsg);
                RETURN NULL;
            }
            
            String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
            String url = salesforceHost + '/services/data/v34.0/sobjects/ApexPage';
            
            if (selectedRecordType != 'All')
            {
                isRecordtypeAvailable = true;
                recordtypeLabel = recordTypeIdNameMap.get(selectedRecordType);
            }
            pageCode = '<apex:page standardController="' + selectedTargetObjectApi + '">' + '<c:StatusBarComponent recordId="{!' + selectedTargetObjectApi + '.id}" objectType="' + selectedTargetObjectApi + '"';
            
            if (isRecordtypeAvailable)
                pageCode = pageCode + ' recordtypeName="{!' + selectedTargetObjectApi + '.RecordType}"';

            pageCode = pageCode + ' fieldNameAttribute="' + selectedPicklistField + '" fieldValueAttribute="{!' + selectedTargetObjectApi + '.' + selectedPicklistField + '}" />' + '</apex:page>';
            
            HttpRequest req = new HttpRequest();
            req.setMethod('POST');
            req.setEndpoint(url);
            req.setHeader('Content-type', 'application/json');
            req.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionId());
            req.setBody('{"Name" : "' + PageName + '","Markup" : "' + pageCode.replaceAll('"', '\'') + '","ControllerType" : "1","MasterLabel":"' + PageName + '","ApiVersion":"34.0"}');
            Http http = new Http();
            
            HTTPResponse res;
            if(!Test.isRunningTest())
            res = http.send(req);
            
            if (Test.isRunningTest() || res.getStatus() == 'Created')
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, LABEL.PAGE_CREATION_SUCCESS_MESSAGE));
                
                lstExistingApexPage = [SELECT Id, Name FROM ApexPage WHERE Name =: pageName LIMIT 1];
                if (!lstExistingApexPage.isEmpty())
                {
                    objFlowConfig.Page_Id__c = lstExistingApexPage[0].Id;
                }
                
                INSERT objFlowConfig;
                ResetAll();
                RETURN NULL;
            }
            else
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, LABEL.PAGE_CREATION_FAILURE_MESSAGE));
                RETURN NULL;
            }
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
            RETURN NULL;
        }
    }
    
    public void ResetAll()
    {
        try
        {
            isEdit = false;
            flowConfigList.clear();
            objFlowConfig = new Flow_Configuration__c();
            objFlowConfig.is_active__c = true;
            selectedTargetObject = NULL;
            selectedTargetObjectApi = NULL;
            selectedRecordType = NULL;
            selectedPicklistField = NULL;
            picklistLeftValues = NULL;
            picklistRightValues = NULL;
            picklistAllRightValues = NULL;
            objFlowConfig.Color_Completed_Stage__c = '#008000';
            objFlowConfig.Color_Current_Stage__c = '#0000FF';
            objFlowConfig.Color_Pending_Stage__c = '#FF0000';
            generatePageName();
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void GeneratePage()
    {
        try
        {
            showPageCode = true;
            pageCode = '<apex:page standardController="' + selectedTargetObjectApi + '">' + '<c:StatusBarComponent recordId="{!' + selectedTargetObjectApi + '.id}" objectType="' + selectedTargetObjectApi + '"';
            if (isRecordtypeAvailable)
            {
                pageCode = pageCode + ' recordtypeName="{!' + selectedTargetObjectApi + '.RecordType}"';
            }
            pageCode = pageCode + ' fieldNameAttribute="' + selectedPicklistField + '" fieldValueAttribute="{!' + selectedTargetObjectApi + '.' + selectedPicklistField + '}" />' + '</apex:page>';
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void GeneratePageName()
    {
        try
        {
            if (selectedPicklistField != NULL && selectedPicklistField != '')
            {
                PageName = selectedTargetObject + '_' + fieldApiFieldNameMap.get(selectedPicklistField) + '_' + 'Indicator';
                PageName = PageName.replaceAll(' ', '');
                if (PageName.length() > 40)
                {
                    PageName = PageName.subString(0, 40);
                }
            }
            else PageName = '';
            pageCode = '';
            showPageCode = false;
            lstExistingApexPage.clear();
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public pageReference RedirectToExistingPage()
    {
        pageReference oPage = new Pagereference('/' + lstExistingApexPage[0].id);
        oPage.setRedirect(false);
        RETURN oPage;
    }
    
    public void EditExistingConfiguration()
    {
        try
        {
            isEdit = true;
            Id oConfigId = System.currentPageReference().getParameters().get('configId');
            objFlowConfig = mapIdExistingFlowConfig.get(oConfigId);
            selectedRecordType = objFlowConfig.Target_Object_Record_Type__c;
            selectedPicklistField = objFlowConfig.Target_Field_API_Name__c;
            set < string > listSelectedValues = new set < String > ();
            listSelectedValues.addAll(objFlowConfig.Target_Field_Values__c.split(';'));
            picklistLeftValues = '';
            picklistRightValues = '';
            picklistAllRightValues = '';
            Schema.SObjectType thisSObject = objectNameObjectTypeMap.get(selectedTargetObjectApi);
            
            if (thisSObject != NULL && selectedPicklistField != NULL && selectedPicklistField != '')
            {
                Schema.SObjectField selectedField = thisSObject.getDescribe().fields.getMap().get(selectedPicklistField);
                for (Schema.PicklistEntry objPicklistItem: selectedField.getDescribe().getPicklistValues())
                {
                    if (!listSelectedValues.contains(objPicklistItem.getLabel())) picklistLeftValues += objPicklistItem.getLabel() + '|' + objPicklistItem.getValue() + ';';
                }
                for (String sRightValue: objFlowConfig.Target_Field_Values__c.split(';'))
                {
                    picklistRightValues += sRightValue + ';';
                    picklistAllRightValues += sRightValue + '|' + sRightValue + ';';
                }
            }
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
    
    public void UpdateConfig()
    {
        try
        {
            objFlowConfig.Target_Field_Values__c = picklistRightValues;
            UPSERT objFlowConfig;
            flowConfigList.clear();
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Configuration successfully updated.'));
            ResetAll();
        }
        catch (Exception ex)
        {
            system.debug('ex-------->' + ex);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }
}
global class BatchUpload implements Database.Batchable<case> 
       {
           //Vignesh Committing...               
            global List<case> tobeuploaded=new List<case>();
            global List<case> tobeuploadedinbatch=new List<case>();
            
            global Set<String> allerr=new Set<String>();
            global Set<String> partialerr=new Set<String>();
            global List<String> finalmsgsofbatch=new List<String>();
            
            public Database.SaveResult[] srList;
            
            public integer successCount=0;
            public integer failCount=0;
            public integer updatedCount=0;
            public integer insertedCount=0;
            public set<Id> successRecordSet = null;

            
            global integer boo;
            global integer o=0;                
            global String msg;
            public String fail1msg;
            public String fail2msg;
            public integer countsuccess=0;
            public integer countfailure1=0;
            public integer countfailure2=0;      
            public integer countfailure3=0;
            global String idofjob;
            List<String> ListOfErrorMessages =new List<String>();
            //Default Constructor
            global BatchUpload()
            {
            }
            //Parameterized Constructor.. Getting Values from Controller  class..
            global BatchUpload(List<case> rec)
            {
              system.debug('INSIDE PARAMETERIZED CONSTRUCTOR!!!!!!!!!!!!!');            
              this.tobeuploaded=rec;
              system.debug('B4 EXITING PARAMETERIZED CONSTRUCTOR!!!!!!!!!!'+this.tobeuploaded);
            }
            
            // Start Method     
            global Iterable<case> start(Database.BatchableContext BC)
            {
               return tobeuploaded;
            }
            
            // Execute Logic
            global void execute(Database.BatchableContext BC,List<case> scope)
            { 
                  /*List <Contact> modifiedContacts = new list<Contact>();
                  for(Sobject s : scope)
                  {
                      Contact a = (Contact)s;
                      // Add some information to the contact
                      modifiedContacts.add(a);
                  }
                  update modifiedContacts;
                  */
               // Logic to be Executed batch wise 

          for(Case s : scope)
          {
          Case c=(Case)s;     
          tobeuploadedinbatch.add(c);
          }
      
         try
         {
           srList= Database.update(tobeuploadedinbatch,false);
          
            for (Database.SaveResult sr : srList) 
            { 
             if (sr.isSuccess()) 
             {    
             o++;
             countsuccess=1;
             }
              
             else 
             {
             system.debug('!!!!!!!!!!!VALUE OF O'+o);
             // Operation failed, getting all the errors.
              for(Database.Error err : sr.getErrors()) 
               {  
               if(o==0)
                {
                 countfailure1=1;
                 allerr.add(err.getMessage());
                }
                else
                {
                countfailure2=1;
                partialerr.add(err.getMessage());
                }
               }
             } 
            }
       }
       catch(exception e)
       {
           countfailure3=1;
           msg='Updation Unsuccessful!!. Please Refresh the page for further uploading.';
       }
    system.debug('!!!!!!!!SUCCESS&FAIL COUNT!!!!!!!!!'+countsuccess+'!!!'+countfailure1+'!!!'+countfailure2+'!!!'+countfailure3);
    system.debug('!!!!!!!VALUE OF O OUTSIDE TRY/CATCH!!!!!!'+o);
    system.debug('!!!!!!VALUE OF SRLIST.SIZE()!!!!!!!'+srList.size());
    if(o>0)
    {
    msg='Total '+o+' records were uploaded and updated successfully. Please refresh the page for subsequent updates.';
    system.debug('!!!!!'+msg);
    finalmsgsofbatch.add(msg);
    }
    if(o==0 && countfailure1==1)
    {
    msg='The following error has occured on the records. Error Message:'+allerr;
    system.debug('!!!!!'+msg);
    finalmsgsofbatch.add(msg);
    }

    if((o>0 && (o<srList.size())) && countfailure2==1)
    {
    msg='The following error has occured on the remaining records. Error Message:'+partialerr;
    system.debug('!!!!!'+msg);
    finalmsgsofbatch.add(msg);
    }

    if(countfailure3==1)
    {
    msg='Updation Unsuccessful!!. Please Refresh the page for further uploading.';
    system.debug('!!!!!'+msg);
    finalmsgsofbatch.add(msg);
    }
    system.debug('FINAL MSG!!!!!!!!!!!!!'+finalmsgsofbatch);
    
    }
   
   
    // Logic to be Executed at finish
    global void finish(Database.BatchableContext BC)
    {
    system.debug('!!!!!!!!!!!!Inside Final !!!!!!!!!');
    
    BatchUpdate_info.Updateresult = true;
    system.debug('Batch process result ' + BatchUpdate_info.Updateresult);
    
    idofjob=(String)BC.getJobId();
    system.debug('!!!!!!!!!!!!!!!!!'+idofjob);
    AsyncApexJob a = [SELECT Id,ApexClassID,CompletedDate, Status, NumberOfErrors, JobItemsProcessed,
      TotalJobItems,JobType,MethodName FROM AsyncApexJob WHERE Id =:idofjob];
      system.debug('!!!!!!!!!INSIDE FINAL!!!QUERY RESULT!!!!!!!!!!!!!'+a);
      system.debug('FINAL MESSAGES!!!!!!'+finalmsgsofbatch);
      if(a.status=='completed')
      {
      system.debug('BATCH COMPLETED!!!!!!!!!DO OTHER WORKS!!!!!!!!!!');      
      }
      
     /* Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String body = 'Hi ,<br/> Approval Process is completed. <br/>';
        
        if(ListOfErrorMessages.size()>0)
            body += '<br/>TOtal Batch failed ==>'+a.NumberOfErrors+'<br/>Following Error were addressed: <br/>';
       
        for(Id msg : ListOfErrorMessages)
        {
            body += 'Error message ==> '+msg+'<br/>';
        }
       
        mail.setTargetObjectId(userInfo.getuserId());
        mail.setSubject('Batch Compeletion Report');      
        mail.setSaveAsActivity(false);
        mail.setHtmlBody(body);
        Messaging.SendEmail(new Messaging.SingleEmailMessage[] { mail });
        */    
     }
 }